# da-scripts

This is a collection of general purpose scripts and tools are useful to have on
DirectAdmin servers.

The Gitlab pipeline and build related files are used internally, and might not
make sense for anyone else.

License: GPL 3.0

## ns-check

This script will loop through all domains found in /etc/virtual/domainowners and
look up the NS record for each domain. It will then print each domain with
information about what was found. The NS records will be compared with the ones
from the domain you supply as the first argument, and consider those that match
as "internal". This might be useful to detect domains that have been moved away.

## php-version-selections

A script to show, save, and restore selection of the PHP version for each
domain. Its intented use case is when there's a need to upgrade the PHP versions
installed on the server, or change the default PHP version. It allows you to
save the current version used by domains, and then after options.conf is
updated, modify selections to ensure the same (if possible) version of PHP is
used, even though the release number has changed. See comments in the script for
more details. The script also supports subdomains with different version
selections than the main domain. A dry run can be achieved by passing any string
as the second argument when using a selection file.

## da-site-backup

A php script that will trigger a DirectAdmin backup run for the user/domain and
contents as defined in the script. This is a very simple script, and if you
want to run this against multiple users/domains, you will need multiple copies
since the config is in the script itself.

## da-dyndns

A bash script that will update a DNS A record for the configured host/domain, in
order to function as a "dynamic dns" updater.
